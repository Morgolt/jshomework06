/* 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

Щоб використовувати спеціальний символ як звичайний, додайте до нього обернену косу риску: \.
Це називається "екранування символу"

2. Які засоби оголошення функцій ви знаєте?

1) Функції вида "function declaration statement":

function square(number) {
  return number * number;
}

2) Функції вида "function definition expression":

let square = function(number) { return number * number; };
let x = square(4); // x отримує значення 16

3) "Стрілочні функції"

let double = n => n * 2;
// те саме, що й: let double = function(n) { return n * 2 }

3. Що таке hoisting, як він працює для змінних та функцій?

Підняття або hoisting - це механізм JavaScript, в якому змінні і оголошення функцій,
пересуваються вгору своєї області видимості перед тим, як код буде виконано.

*/

function createNewUser () {
  const firstName = prompt("Введіть Ваше ім'я");
  const lastName = prompt('Введіть Ваше прізвище');
  const birthday = prompt('Введіть дату Вашого народження у форматі: день.місяць.рік');
  const newUser = {};
  Object.defineProperties(newUser, {
    firstName: {
      value: firstName
    },
    lastName: {
      value: lastName
    },
    getLogin: {
      value: function () {
        const user = this.firstName[0] + this.lastName;
        return user.toLowerCase();
      }
    },

    getAge: {
      value: function () {
        const birthdayD = birthday.split('.');
        return 2022 - birthdayD[2];
      }
    },

    getPassword: {
      value: function () {
        return (firstName[0].toUpperCase() + lastName.toLowerCase() + birthday.split('.')[2]);
      }
    }
  });
  return newUser;
};

const userDB = createNewUser();
console.log(userDB);
console.log(userDB.getAge());
console.log(userDB.getPassword());
